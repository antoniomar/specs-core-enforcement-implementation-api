package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.service.RemPlanService;
import eu.specs.project.enforcement.implementation_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@Controller
@RequestMapping(value = "/rem-plans")
public class RemPlanController {

    @Autowired
    private RemPlanService remPlanService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<RemPlan> createImplActivity(@RequestBody RemPlan remPlanData, HttpServletRequest request) {

        RemPlan remPlan = remPlanService.implementRemPlan(remPlanData);

        URI location = URI.create(request.getRequestURL().append("/").append(remPlan.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<RemPlan>(remPlan, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{remPlanId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public RemPlan getRemPlan(@PathVariable("remPlanId") String remPlanId) throws ResourceNotFoundException {
        RemPlan remPlan = remPlanService.getRemediationPlan(remPlanId);
        if (remPlan == null) {
            throw new ResourceNotFoundException();
        } else {
            return remPlan;
        }
    }
}
