package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;

@Controller
@RequestMapping(value = "/impl-plans")
public class ImplPlanController {

    @Autowired
    private ImplPlanService implPlanService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity storeImplPlan(@RequestBody ImplementationPlan implPlan, HttpServletRequest request) throws IOException {
        implPlanService.storeImplPlan(implPlan);

        URI location = URI.create(request.getRequestURL().append("/").append(implPlan.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }
}
