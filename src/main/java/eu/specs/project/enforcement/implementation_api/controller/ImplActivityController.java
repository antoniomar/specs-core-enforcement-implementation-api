package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.project.enforcement.implementation.core.exception.NotFoundException;
import eu.specs.project.enforcement.implementation.core.service.ImplActivityService;
import eu.specs.project.enforcement.implementation_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/impl-activities")
public class ImplActivityController {

    @Autowired
    private ImplActivityService implActivityService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<ImplActivity> createImplActivity(@RequestBody ImplActivity implActivityData, HttpServletRequest request) throws IOException {
        ImplActivity implActivity = implActivityService.implementPlan(implActivityData);

        URI location = URI.create(request.getRequestURL().append("/").append(implActivity.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(implActivity, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{implActId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ImplActivity getImplActivity(@PathVariable("implActId") String implActId) throws ResourceNotFoundException {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException();
        } else {
            return implActivity;
        }
    }

    @RequestMapping(value = "/{implActId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteImplActivity(@PathVariable("implActId") String implActId) throws NotFoundException {
        implActivityService.deleteImplActivity(implActId);
    }

    @RequestMapping(value = "/{implActId}/state", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Map getImplActivityStatus(@PathVariable("implActId") String implActId) throws ResourceNotFoundException {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException();
        } else {
            Map map = new HashMap();
            map.put("state", implActivity.getState().name());
            return map;
        }
    }
}
